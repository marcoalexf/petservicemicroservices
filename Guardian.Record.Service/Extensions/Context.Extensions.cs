using Guardian.Record.Service.Contexts;
using Microsoft.EntityFrameworkCore;
using Play.Record.Service.Settings;

namespace Guardian.Record.Service.Extensions
{
    public static class ContextExtensions
    {
        public static IServiceCollection AddPostgreSQL(this IServiceCollection services)
        {
            services.AddDbContext<GuardianDataContext>((serviceProvider, options) =>
            {
                var configuration = serviceProvider.GetService<IConfiguration>();
                var dbSettings = configuration.GetSection(nameof(PostgreSQLSettings)).Get<PostgreSQLSettings>();

                options.UseNpgsql(dbSettings.ConnectionString);
            });

            return services;
        }
    }
}