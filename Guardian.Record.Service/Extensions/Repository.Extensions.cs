using Guardian.Record.Service.Dtos;
using Guardian.Record.Service.Repositories;
using Microsoft.EntityFrameworkCore;
using Play.Record.Service.Settings;

namespace Pet.Record.Service.Extensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IGuardianRepository, GuardianRepository>();

            return services;
        }
    }
}