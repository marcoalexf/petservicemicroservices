using Guardian.Record.Common.Entities;
using Guardian.Record.Service.Dtos;
using Guardian.Record.Service.Entities;

namespace Guardian.Record.Service.Extensions
{
    public static class Extensions
    {
        public static GuardianRecord ToEntity(this CreateGuardianDto dto) => new GuardianRecord
        (
            Guid.NewGuid(),
            dto.Name,
            dto.Address,
            dto.NIF,
            dto.Email
        );

        public static GuardianDto ToDto(this IEntity pet)
        {
            var casted = pet as GuardianRecord;
            var res = new GuardianDto
            {
                Id = casted.Id,
                Address = casted.Address,
                Email = casted.Email,
                Name = casted.Name,
                NIF = casted.NIF
            };

            return res;
        }
    }
}