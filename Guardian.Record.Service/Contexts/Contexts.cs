using Guardian.Record.Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Guardian.Record.Service.Contexts
{
    public class GuardianDataContext : DbContext
    {
        public GuardianDataContext(DbContextOptions<GuardianDataContext> options) : base(options) { }

        public DbSet<GuardianRecord> Guardians { get; set; }
    }
}