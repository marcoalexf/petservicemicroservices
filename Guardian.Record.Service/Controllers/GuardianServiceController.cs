using Guardian.Record.Common.Entities;
using Guardian.Record.Service.Dtos;
using Guardian.Record.Service.Extensions;
using Guardian.Record.Service.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Guardian.Record.Service.Controllers;

[ApiController]
[Route("[controller]")]
public class GuardianServiceController : ControllerBase
{
    private readonly ILogger<GuardianServiceController> _logger;
    private IGuardianRepository repository;

    public GuardianServiceController(ILogger<GuardianServiceController> logger, IGuardianRepository repository)
    {
        _logger = logger;
        this.repository = repository;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<GuardianDto>> GetGuardianById(Guid id)
    {

        _logger.LogInformation($"GetGuardianById: {id}");
        var Guardian = await repository.GetAsync(id);

        if (Guardian is not null)
        {
            _logger.LogInformation($"Guardian found: {Guardian.Name}. ID: {Guardian.Id}");
            return Ok(Guardian.ToDto());
        }

        _logger.LogInformation($"Guardian not found: {id}");
        return NotFound();
    }

    [HttpGet(Name = "GetGuardians")]
    public async Task<ActionResult<List<GuardianDto>>> GetGuardians()
    {
        return Ok(await repository.GetAllAsync());
    }

    [HttpPost(Name = "CreateGuardian")]
    public async Task<IActionResult> CreateGuardian(CreateGuardianDto createGuardianDto)
    {
        _logger.LogInformation($"Creating Guardian. Transforming {createGuardianDto.ToString()} to {nameof(GuardianRecord)}");
        await repository.CreateAsync(createGuardianDto.ToEntity());
        _logger.LogInformation($"Created Guardian with name {createGuardianDto.Name}");

        // TODO: Implement repository
        return CreatedAtAction(nameof(GetGuardianById), new { id = createGuardianDto.ToEntity().Id }, createGuardianDto.ToEntity());
    }

    [HttpPut(Name = "UpdateGuardian")]
    public async Task<IActionResult> UpdateGuardian(UpdateGuardianDto updateGuardianDto)
    {
        _logger.LogInformation($"Updating Guardian with id {updateGuardianDto.Id}");
        var GuardianToUpdate = await repository.GetAsync(updateGuardianDto.Id);
        if (GuardianToUpdate is not null)
        {
            _logger.LogInformation($"Guardian with id {updateGuardianDto.Id} found, updating.");
            var updatedGuardian = GuardianToUpdate with
            {
                Name = updateGuardianDto.Name,
                Address = updateGuardianDto.Address,
                NIF = updateGuardianDto.NIF,
                Email = updateGuardianDto.Email,
            };

            await repository.UpdateAsync(updatedGuardian);

            return AcceptedAtAction(nameof(GetGuardianById), new { id = updatedGuardian.Id }, updatedGuardian);
        }
        else
        {
            _logger.LogInformation($"Guardian with id {updateGuardianDto.Id} not found.");
            return NotFound();
        }
    }

    [HttpDelete(Name = "DeleteGuardian")]
    public async Task<IActionResult> DeleteGuardian(Guid id)
    {
        _logger.LogInformation($"Received DELETE request for Guardian with id {id}");
        var GuardianToDelete = repository.GetAsync(id);
        if (GuardianToDelete is not null)
        {
            _logger.LogInformation($"Deleting Guardian with id {id}");
            await repository.RemoveAsync(id);
            return Ok();
        }
        else
        {
            _logger.LogInformation($"Guardian with id {id} not found");
            return NotFound();
        }
    }
}
