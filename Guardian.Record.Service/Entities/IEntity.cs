namespace Guardian.Record.Service.Entities
{
    public interface IEntity
    {
        Guid Id { get; init; }
    }
}