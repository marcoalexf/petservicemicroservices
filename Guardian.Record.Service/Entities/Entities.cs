using System;
using System.ComponentModel.DataAnnotations;
using Guardian.Record.Service.Entities;

namespace Guardian.Record.Common.Entities
{
    public record GuardianRecord
    (
        Guid Id,
        string Name,
        string Address,
        string NIF,
        string Email
    ) : IEntity;
}