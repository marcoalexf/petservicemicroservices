namespace Guardian.Record.Service.Dtos
{
    public record GuardianDto : IGuardianDto
    {
        public Guid Id { get; init; }
        public string Name { get; init; }
        public string Address { get; init; }
        public string NIF { get; init; }
        public string Email { get; init; }
    }
}