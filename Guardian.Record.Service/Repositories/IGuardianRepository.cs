using Guardian.Record.Common.Entities;

namespace Guardian.Record.Service.Repositories
{
    public interface IGuardianRepository : IRepository<GuardianRecord>
    {
        Task<GuardianRecord> CreateAsync(GuardianRecord entity);
        Task<IReadOnlyCollection<GuardianRecord>> GetAllAsync();
        Task<GuardianRecord> GetAsync(Guid id);
        Task<int> RemoveAsync(Guid id);
        Task<GuardianRecord> UpdateAsync(GuardianRecord entity);
    }
}