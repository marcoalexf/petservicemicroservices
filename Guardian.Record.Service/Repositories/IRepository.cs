using System.Linq.Expressions;
using Guardian.Record.Service.Entities;

namespace Guardian.Record.Service.Repositories
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        Task<TEntity> CreateAsync(TEntity entity);
        Task<IReadOnlyCollection<TEntity>> GetAllAsync();
        Task<TEntity> GetAsync(Guid id);
        Task<int> RemoveAsync(Guid id);
        Task<TEntity> UpdateAsync(TEntity entity);
    }
}