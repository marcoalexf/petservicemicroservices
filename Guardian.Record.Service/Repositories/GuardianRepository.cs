using Guardian.Record.Common.Entities;
using Guardian.Record.Service.Contexts;
using Guardian.Record.Service.Repositories;

namespace Guardian.Record.Service.Repositories
{
    public class GuardianRepository : Repository<GuardianRecord, GuardianDataContext>, IGuardianRepository
    {
        public GuardianRepository(GuardianDataContext context) : base(context) { }
    }
}