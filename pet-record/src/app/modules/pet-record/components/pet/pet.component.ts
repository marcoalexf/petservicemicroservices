import { Component, OnInit } from '@angular/core';
import { PetsService } from '../../controllers/pets.service';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.scss']
})
export class PetComponent implements OnInit {

  constructor(private controller: PetsService) { }

  ngOnInit(): void {
  }

}
