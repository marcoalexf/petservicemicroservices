import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from 'src/app/shared/shared-components/list/list.component';
import { PetComponent } from './components/pet/pet.component';

const routes: Routes = [
  {
    path: '',
    component: PetComponent,
    children: [
      {
        path: '',
        component: ListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PetRecordRoutingModule { }
