import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetRecordRoutingModule } from './pet-record-routing.module';
import { PetComponent } from './components/pet/pet.component';


@NgModule({
  declarations: [
  
    PetComponent
  ],
  imports: [
    CommonModule,
    PetRecordRoutingModule
  ]
})
export class PetRecordModule { }
