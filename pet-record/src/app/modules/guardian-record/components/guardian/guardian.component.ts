import { Component, OnInit } from '@angular/core';
import { GuardianService } from '../../controllers/guardian.service';

@Component({
  selector: 'app-guardian',
  templateUrl: './guardian.component.html',
  styleUrls: ['./guardian.component.scss']
})
export class GuardianComponent implements OnInit {

  constructor(private controller: GuardianService) { }

  ngOnInit(): void {
    console.error('guardian master component');
  }

}
