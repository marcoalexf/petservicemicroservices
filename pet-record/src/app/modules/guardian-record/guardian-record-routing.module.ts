import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from 'src/app/shared/shared-components/list/list.component';
import { GuardianComponent } from './components/guardian/guardian.component';

const routes: Routes = [
  {
    path: '',
    component: GuardianComponent,
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'detail',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuardianRecordRoutingModule { }
