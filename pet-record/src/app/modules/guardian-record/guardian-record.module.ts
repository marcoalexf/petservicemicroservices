import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuardianRecordRoutingModule } from './guardian-record-routing.module';
import { GuardianComponent } from './components/guardian/guardian.component';


@NgModule({
  declarations: [
  
    GuardianComponent
  ],
  imports: [
    CommonModule,
    GuardianRecordRoutingModule
  ]
})
export class GuardianRecordModule { }
