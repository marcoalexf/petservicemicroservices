import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { AddEditFormComponent } from './add-edit-form/add-edit-form.component';



@NgModule({
  declarations: [
    ListComponent,
    AddEditFormComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedComponentsModule { }
