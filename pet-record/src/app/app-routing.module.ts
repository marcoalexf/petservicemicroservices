import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'pets',
    loadChildren: () => import('./modules/pet-record/pet-record.module').then(m => m.PetRecordModule)
  },
  {
    path: 'guardians',
    loadChildren: () => import('./modules/guardian-record/guardian-record.module').then(m => m.GuardianRecordModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
