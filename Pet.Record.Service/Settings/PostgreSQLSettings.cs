namespace Play.Record.Service.Settings
{
    public class PostgreSQLSettings
    {
        public string Server { get; init; }
        public int Port { get; init; }
        public string Database { get; init; }
        public string UserId { get; init; }
        public string Password { get; init; }

        public string ConnectionString => $"Server={Server};Port={Port};Database={Database};User Id={UserId};Password={Password};";
    }
}