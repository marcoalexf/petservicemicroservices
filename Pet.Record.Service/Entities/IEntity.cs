namespace Pet.Record.Service.Entities
{
    public interface IEntity
    {
        Guid Id { get; init; }
    }
}