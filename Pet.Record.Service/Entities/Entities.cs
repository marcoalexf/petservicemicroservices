using System;
using System.ComponentModel.DataAnnotations;
using Pet.Record.Service.Entities;
using Pet.Record.Service.Enums;

namespace Pet.Record.Common.Entities
{
    public record PetRecord
    (
        Guid Id,
        string Name,
        PET_SPECIES Species,
        string Breed,
        string Color,
        Guid GuardianId,
        GENDER_ENUM Gender,
        Guid VaccinationPlanId,
        DateTime Birthday
    ) : IEntity;
}