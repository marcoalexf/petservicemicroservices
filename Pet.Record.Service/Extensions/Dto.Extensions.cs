using Pet.Record.Common.Entities;
using Pet.Record.Service.Dtos;
using Pet.Record.Service.Entities;

namespace Pet.Record.Service.Extensions
{
    public static class Extensions
    {
        public static PetRecord ToEntity(this CreatePetDto dto) => new PetRecord
        (
            Guid.NewGuid(),
            dto.Name,
            dto.Species,
            dto.Breed,
            dto.Color,
            dto.GuardianId,
            dto.Gender,
            Guid.Empty,
            dto.Birthday
        );

        public static PetDto ToDto(this IEntity pet)
        {
            var casted = pet as PetRecord;
            var res = new PetDto
            {
                Id = casted.Id,
                Birthday = casted.Birthday,
                Breed = casted.Breed,
                Color = casted.Color,
                GuardianId = casted.GuardianId,
                VaccinationPlanId = casted.VaccinationPlanId,
                Gender = casted.Gender,
                Name = casted.Name,
                Species = casted.Species
            };

            return res;
        }
    }
}