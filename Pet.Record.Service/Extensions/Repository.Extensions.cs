using Microsoft.EntityFrameworkCore;
using Pet.Record.Common.Contexts;
using Pet.Record.Service.Dtos;
using Pet.Record.Service.Repositories;
using Play.Record.Service.Settings;

namespace Pet.Record.Service.Extensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IPetRepository, PetRepository>();

            return services;
        }
    }
}