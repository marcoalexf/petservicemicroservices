using Microsoft.EntityFrameworkCore;
using Pet.Record.Common.Contexts;
using Play.Record.Service.Settings;

namespace Pet.Record.Service.Extensions
{
    public static class ContextExtensions
    {
        public static IServiceCollection AddPostgreSQL(this IServiceCollection services)
        {
            services.AddDbContext<PetDataContext>((serviceProvider, options) =>
            {
                var configuration = serviceProvider.GetService<IConfiguration>();
                var dbSettings = configuration.GetSection(nameof(PostgreSQLSettings)).Get<PostgreSQLSettings>();

                options.UseNpgsql(dbSettings.ConnectionString);
            });

            return services;
        }
    }
}