using Microsoft.AspNetCore.Mvc;
using Pet.Record.Common.Contexts;
using Pet.Record.Common.Entities;
using Pet.Record.Service.Dtos;
using Pet.Record.Service.Extensions;
using Pet.Record.Service.Repositories;

namespace Pet.Record.Service.Controllers;

[ApiController]
[Route("[controller]")]
public class PetServiceController : ControllerBase
{
    private readonly ILogger<PetServiceController> _logger;
    private IPetRepository repository;

    public PetServiceController(ILogger<PetServiceController> logger, IPetRepository repository)
    {
        _logger = logger;
        this.repository = repository;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<PetDto>> GetPetById(Guid id)
    {

        _logger.LogInformation($"GetPetById: {id}");
        var pet = await repository.GetAsync(id);

        if (pet is not null)
        {
            _logger.LogInformation($"Pet found: {pet.Name}. ID: {pet.Id}");
            return Ok(pet.ToDto());
        }

        _logger.LogInformation($"Pet not found: {id}");
        return NotFound();
    }

    [HttpGet(Name = "GetPets")]
    public async Task<ActionResult<List<PetDto>>> GetPets()
    {
        return Ok(await repository.GetAllAsync());
    }

    [HttpPost(Name = "CreatePet")]
    public async Task<IActionResult> CreatePet(CreatePetDto createPetDto)
    {
        _logger.LogInformation($"Creating pet. Transforming {createPetDto.ToString()} to {nameof(PetRecord)}");
        await repository.CreateAsync(createPetDto.ToEntity());
        _logger.LogInformation($"Created pet with name {createPetDto.Name}, species {createPetDto.Species}, breed {createPetDto.Breed}");

        // TODO: Implement repository
        return CreatedAtAction(nameof(GetPetById), new { id = createPetDto.ToEntity().Id }, createPetDto.ToEntity());
    }

    [HttpPut(Name = "UpdatePet")]
    public async Task<IActionResult> UpdatePet(UpdatePetDto updatePetDto)
    {
        _logger.LogInformation($"Updating pet with id {updatePetDto.Id}");
        var petToUpdate = await repository.GetAsync(updatePetDto.Id);
        if (petToUpdate is not null)
        {
            _logger.LogInformation($"Pet with id {updatePetDto.Id} found, updating.");
            var updatedPet = petToUpdate with
            {
                Name = updatePetDto.Name,
                Breed = updatePetDto.Breed,
                Color = updatePetDto.Color,
                GuardianId = updatePetDto.GuardianId,
            };

            await repository.UpdateAsync(updatedPet);

            return AcceptedAtAction(nameof(GetPetById), new { id = updatedPet.Id }, updatedPet);
        }
        else
        {
            _logger.LogInformation($"Pet with id {updatePetDto.Id} not found.");
            return NotFound();
        }
    }

    [HttpDelete(Name = "DeletePet")]
    public async Task<IActionResult> DeletePet(Guid id)
    {
        _logger.LogInformation($"Received DELETE request for Pet with id {id}");
        var petToDelete = repository.GetAsync(id);
        if (petToDelete is not null)
        {
            _logger.LogInformation($"Deleting Pet with id {id}");
            await repository.RemoveAsync(id);
            return Ok();
        }
        else
        {
            _logger.LogInformation($"Pet with id {id} not found");
            return NotFound();
        }
    }
}
