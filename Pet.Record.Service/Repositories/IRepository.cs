using System.Linq.Expressions;
using Pet.Record.Common.Entities;
using Pet.Record.Service.Dtos;
using Pet.Record.Service.Entities;

namespace Pet.Record.Service.Repositories
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        Task<TEntity> CreateAsync(TEntity entity);
        Task<IReadOnlyCollection<TEntity>> GetAllAsync();
        Task<TEntity> GetAsync(Guid id);
        Task<int> RemoveAsync(Guid id);
        Task<TEntity> UpdateAsync(TEntity entity);
    }
}