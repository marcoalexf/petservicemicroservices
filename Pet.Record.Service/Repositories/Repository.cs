using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Pet.Record.Common.Contexts;
using Pet.Record.Common.Entities;
using Pet.Record.Service.Dtos;
using Pet.Record.Service.Entities;
using Pet.Record.Service.Extensions;

namespace Pet.Record.Service.Repositories
{
    public class Repository<TEntity, V> : IRepository<TEntity> where TEntity : class, IEntity where V : DbContext
    {

        private readonly V context;
        public Repository(V context)
        {
            this.context = context;
        }
        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            var created = await context.Set<TEntity>().AddAsync(entity).AsTask();
            await context.SaveChangesAsync();
            return created.Entity;
        }

        public async Task<IReadOnlyCollection<TEntity>> GetAllAsync()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetAsync(Guid id)
        {
            return await context.Set<TEntity>().Where(p => p.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task<int> RemoveAsync(Guid id)
        {
            var entityToRemove = context.Set<TEntity>().Where(p => p.Id.Equals(id)).FirstOrDefaultAsync();
            context.Remove(entityToRemove);
            return await context.SaveChangesAsync();
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity is null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            var updated = context.Set<TEntity>().Update(entity);

            await context.SaveChangesAsync();

            return updated.Entity;
        }
    }
}