using Pet.Record.Common.Contexts;
using Pet.Record.Common.Entities;
using Pet.Record.Service.Extensions;

namespace Pet.Record.Service.Repositories
{
    public class PetRepository : Repository<PetRecord, PetDataContext>, IPetRepository
    {
        public PetRepository(PetDataContext context) : base(context) { }
    }
}