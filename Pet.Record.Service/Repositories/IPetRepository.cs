using Pet.Record.Common.Entities;

namespace Pet.Record.Service.Repositories
{
    public interface IPetRepository : IRepository<PetRecord>
    {
        Task<PetRecord> CreateAsync(PetRecord entity);
        Task<IReadOnlyCollection<PetRecord>> GetAllAsync();
        Task<PetRecord> GetAsync(Guid id);
        Task<int> RemoveAsync(Guid id);
        Task<PetRecord> UpdateAsync(PetRecord entity);
    }
}