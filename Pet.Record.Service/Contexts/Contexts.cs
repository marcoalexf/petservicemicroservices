using Microsoft.EntityFrameworkCore;
using Pet.Record.Common.Entities;

namespace Pet.Record.Common.Contexts
{
    public class PetDataContext : DbContext
    {
        public PetDataContext(DbContextOptions<PetDataContext> options) : base(options) { }

        public DbSet<PetRecord> Pets { get; set; }
    }
}